use reqwest::blocking::Client;
use reqwest::StatusCode;

fn send() -> Result<String, reqwest::Error>
{
    let client = Client::new();
    let res = client.post("http://localhost:8890/sparql")
    .body("select distinct ?Concept where {[] a ?Concept} LIMIT 100")
    .header("Content-Type", "application/sparql-update")
    .header("Accept", "text/html")
    .send()?;

    match res.status() 
    {
        StatusCode::OK => {
            println!("success!");
        },
        StatusCode::PAYLOAD_TOO_LARGE => {
            println!("Request payload is too large!");
        },
        s => println!("Received response status: {:?}", s),
    };

    return res.text();
}

fn main() 
{
    println!("Hello, world!");
    
    let res = send();//.ok().as_deref();
    match res {
        Ok(result) => println!("{}", result),
            Err(_) => println!("Something went wrong."),
    };
}

